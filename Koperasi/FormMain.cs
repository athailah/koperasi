﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Koperasi
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var contex = new KOPERASIEntities();
                var siswa = new siswa()
                { 
                        Nim = "01",
                        Nama = "Nur Masyithah Jamil",
                        Kelamin = false,
                        Alamat = "PSR Batam",
                        TempatLahir = "Pekalongan",
                        TanggalLahir = DateTime.Parse("2006/02/01")
                };

                contex.siswas.Add(siswa);
                contex.SaveChanges();
            }catch(Exception exception)
            {
                MessageBox.Show(exception.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
